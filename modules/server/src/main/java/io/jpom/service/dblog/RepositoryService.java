package io.jpom.service.dblog;

import io.jpom.model.data.RepositoryModel;
import io.jpom.service.h2db.BaseDbService;
import org.springframework.stereotype.Service;

/**
 * @author Hotstrip
 * Repository service
 */
@Service
public class RepositoryService extends BaseDbService<RepositoryModel> {

}
