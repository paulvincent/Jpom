# 赞赏公示

| 日期 | 渠道  |  金额  |  昵称 |
|---|---|---|---|
|  2021-09-01 | 微信赞赏码 |  100  | CoCo  |
|  2021-09-01 | 微信赞赏码 |  20  | 大灰灰  |
|  2021-08-31 | 微信赞赏码 |  1  | 大灰灰  |


### 历史赞赏（已经消费）


| 日期 | 渠道  |  金额  |  昵称 |
|---|---|---|---|
|  2021-04-21 | 码云捐赠 |  10  | [jason](https://gitee.com/bwcx_jzy)  |
|  2020-03-31 | 码云捐赠 |  20  | [开源oschina](https://gitee.com/bdj)  |
|  2020-02-25 | 码云捐赠 |  20  | [辣椒酱](https://gitee.com/yokead_admin)  |
|  2020-02-13 | 码云捐赠 |  100  | [大森林](https://gitee.com/jmdhappy)  |
|  2019-08-20 | 码云捐赠 |  15  | [YountMan](https://gitee.com/YountMan)  |
|  2019-07-29 | 码云捐赠 |  50  | [Yashin](https://gitee.com/yashin)  |
|  2019-07-28 | 码云捐赠 |  100  | 老李  |
|  2019-03-27 | 码云捐赠 |  10  | [jason](https://gitee.com/bwcx_jzy)  |